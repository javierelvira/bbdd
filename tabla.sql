DROP PROCEDURE IF EXISTS info;
USE JARDINERIA;

DELIMITER |
CREATE PROCEDURE info()

DECLARE numClientes,numEmpleados,numPedidos INT;

    BEGIN
        DROP TABLE IF EXISTS Info;
        CREATE TABLE Info (NumClientes INT, NumEmpleados INT, NumPedidos INT);

        @numClientes = (SELECT COUNT(*) FROM Clientes);
        @numEmpleados = (SELECT COUNT(*) FROM Empleados);
        @numPedidos = (SELECT COUNT(*) FROM Pedidos);

        INSERT INTO Info VALUES ((SELECT COUNT(*) FROM Clientes),(SELECT COUNT(*) FROM Empleados),(SELECT COUNT(*) FROM Pedidos));

    END;

  |

DELIMITER ;

