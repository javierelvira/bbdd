DROP DATABASE IF EXISTS BBDD;
CREATE DATABASE BBDD;
USE BBDD;

CREATE TABLE MAPA(
NOMBRE VARCHAR(40),
Nº_JUGADORES SMALLINT,
OBJETIVOS VARCHAR(30),

CONSTRAINT PK_MAPA PRIMARY KEY(NOMBRE)
);

CREATE TABLE OBJETO(
NOMBRE VARCHAR(40),
ACTIVACION ENUM('SI','NO'),
COSTE SMALLINT,

CONSTRAINT PK_OBJETO PRIMARY KEY(NOMBRE)
);

CREATE TABLE DINERO(
RP SMALLINT,
ESENCIAS_NARANJAS SMALLINT,
ESENCIAS_AZULOS SMALLINT,

CONSTRAINT PK_DINERO PRIMARY KEY(RP, ESENCIAS_NARANJAS)
);

CREATE TABLE RANGO(
MMR SMALLINT,
DIVISION ENUM('I','II','III','IV'),
LIGA ENUM('HIERRO','BRONCE','PLATA','ORO','PLATINO','DIAMANTE','MASTER','CHALLENGER'),

CONSTRAINT PK_RANGO PRIMARY KEY(MMR)
);


