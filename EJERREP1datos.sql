INSERT INTO PERSONA VALUES ( 'Walter', 'Wilde', 'Profesor'),
			('Jessie', 'Pinkman', 'Dealer'),
			('Saul', 'Guazman', 'Abogado');

INSERT INTO OBJETO VALUES ('Mochila', 'mediano'),
			('Balon','mediano'),
			('Lapiz','pequeño');


INSERT INTO SITUACION VALUES(9, 'caravana', 'Jessie', 'Pinkman','Heisenberg','Baby Blue'),
			(10,'despacho','Saul','Guzman','Traje','Pasta');

INSERT INTO lleva VALUES ('mochila',9,'caravana'),
			('lapiz',10,'despacho');
