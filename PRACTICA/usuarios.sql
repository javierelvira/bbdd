DROP USER IF EXISTS 'admin'@'localhost';
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'patata';
GRANT ALL ON *.* TO 'admin'@'localhost';

DROP USER IF EXISTS 'gerente'@'localhost';
CREATE USER 'gerente'@'localhost' IDENTIFIED BY 'gerente';
GRANT UPDATE, SELECT, DELETE, INSERT ON bbdd.* TO 'gerente'@'localhost';

DROP USER IF EXISTS 'comercial'@'localhost';
CREATE USER 'comercial'@'localhost' IDENTIFIED BY 'comercial';
GRANT SELECT, INSERT, DELETE, UPDATE, RENAME ON  bbdd.* TO 'comercial'@'localhost';

DROP USER IF EXISTS 'cliente'@'localhost';
CREATE USER 'cliente'@'localhost' IDENTIFIED BY 'cliente';
GRANT SELECT, UPDATE(NOMBRE, APELLIDOS, DOMICILIO, DNI) ON bbdd.CLIENTES TO 'cliente'@'localhost';
GRANT SELECT ON bbdd.PRODUCTOS TO 'cliente'@'localhost';
GRANT SELECT ON bbdd.PEDIDOS TO 'cliente'@'localhost';

/*DROP USER IF EXISTS 'cajero'@'localhost';
CREATE USER 'cajero'@'localhost' IDENTIFIED BY 'cajero';
GRANT ALL ON bbdd.* TO 'cajero'@'localhost';*/
