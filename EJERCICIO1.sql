-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: EJERCICIO1
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `APARATO`
--

DROP TABLE IF EXISTS `APARATO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `APARATO` (
  `codigo_unico` char(20) NOT NULL,
  `descripcion` varchar(30) DEFAULT NULL,
  `nombreTIPO` varchar(20) DEFAULT NULL,
  `caracteristicasTIPO` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codigo_unico`),
  KEY `nombreTIPO` (`nombreTIPO`,`caracteristicasTIPO`),
  CONSTRAINT `APARATO_ibfk_1` FOREIGN KEY (`nombreTIPO`, `caracteristicasTIPO`) REFERENCES `TIPO` (`nombre`, `caracteristicas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `APARATO`
--

LOCK TABLES `APARATO` WRITE;
/*!40000 ALTER TABLE `APARATO` DISABLE KEYS */;
/*!40000 ALTER TABLE `APARATO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `COMPONENTES`
--

DROP TABLE IF EXISTS `COMPONENTES`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMPONENTES` (
  `nombre` varchar(20) NOT NULL,
  `especificaciones` varchar(30) NOT NULL,
  PRIMARY KEY (`nombre`,`especificaciones`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMPONENTES`
--

LOCK TABLES `COMPONENTES` WRITE;
/*!40000 ALTER TABLE `COMPONENTES` DISABLE KEYS */;
/*!40000 ALTER TABLE `COMPONENTES` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FABRICADO_POR`
--

DROP TABLE IF EXISTS `FABRICADO_POR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FABRICADO_POR` (
  `nombreCOMPONENTES` varchar(20) DEFAULT NULL,
  `especificacionesCOMPONENTES` varchar(30) DEFAULT NULL,
  `CIFFABRICANTES` char(9) DEFAULT NULL,
  KEY `nombreCOMPONENTES` (`nombreCOMPONENTES`,`especificacionesCOMPONENTES`),
  KEY `CIFFABRICANTES` (`CIFFABRICANTES`),
  CONSTRAINT `FABRICADO_POR_ibfk_1` FOREIGN KEY (`nombreCOMPONENTES`, `especificacionesCOMPONENTES`) REFERENCES `COMPONENTES` (`nombre`, `especificaciones`),
  CONSTRAINT `FABRICADO_POR_ibfk_2` FOREIGN KEY (`CIFFABRICANTES`) REFERENCES `FABRICANTE` (`CIF`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FABRICADO_POR`
--

LOCK TABLES `FABRICADO_POR` WRITE;
/*!40000 ALTER TABLE `FABRICADO_POR` DISABLE KEYS */;
/*!40000 ALTER TABLE `FABRICADO_POR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FABRICANTE`
--

DROP TABLE IF EXISTS `FABRICANTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FABRICANTE` (
  `CIF` char(9) NOT NULL,
  `domicilio_social` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`CIF`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FABRICANTE`
--

LOCK TABLES `FABRICANTE` WRITE;
/*!40000 ALTER TABLE `FABRICANTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `FABRICANTE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LLEVA`
--

DROP TABLE IF EXISTS `LLEVA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LLEVA` (
  `codigo_unicoAPARATO` char(20) DEFAULT NULL,
  `nombreCOMPONENTES` varchar(20) DEFAULT NULL,
  `especificacionesCOMPONENTES` varchar(30) DEFAULT NULL,
  KEY `codigo_unicoAPARATO` (`codigo_unicoAPARATO`),
  KEY `nombreCOMPONENTES` (`nombreCOMPONENTES`,`especificacionesCOMPONENTES`),
  CONSTRAINT `LLEVA_ibfk_1` FOREIGN KEY (`codigo_unicoAPARATO`) REFERENCES `APARATO` (`codigo_unico`),
  CONSTRAINT `LLEVA_ibfk_2` FOREIGN KEY (`nombreCOMPONENTES`, `especificacionesCOMPONENTES`) REFERENCES `COMPONENTES` (`nombre`, `especificaciones`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LLEVA`
--

LOCK TABLES `LLEVA` WRITE;
/*!40000 ALTER TABLE `LLEVA` DISABLE KEYS */;
/*!40000 ALTER TABLE `LLEVA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PERTENECE`
--

DROP TABLE IF EXISTS `PERTENECE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PERTENECE` (
  `general` varchar(20) DEFAULT NULL,
  `subtipo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PERTENECE`
--

LOCK TABLES `PERTENECE` WRITE;
/*!40000 ALTER TABLE `PERTENECE` DISABLE KEYS */;
/*!40000 ALTER TABLE `PERTENECE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TIPO`
--

DROP TABLE IF EXISTS `TIPO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TIPO` (
  `nombre` varchar(20) NOT NULL,
  `caracteristicas` varchar(30) NOT NULL,
  PRIMARY KEY (`nombre`,`caracteristicas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TIPO`
--

LOCK TABLES `TIPO` WRITE;
/*!40000 ALTER TABLE `TIPO` DISABLE KEYS */;
/*!40000 ALTER TABLE `TIPO` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-10  9:39:33
