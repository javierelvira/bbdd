DROP DATABASE IF EXISTS Starwars;
CREATE DATABASE Starwars;
USE Starwars;

CREATE TABLE ACTORES(
Codigo INT AUTO_INCREMENT,
Nombre VARCHAR(30),
Fecha DATE,
Nacionalidad VARCHAR(30) DEFAULT 'Estadounidense',
CONSTRAINT PK_ACTORES PRIMARY KEY (Codigo)
);

CREATE TABLE PERSONAJES(
Codigo INT AUTO_INCREMENT,
Nombre VARCHAR(30),
Raza VARCHAR(30) DEFAULT 'Humano',
Grado SMALLINT UNSIGNED,
Codigo_ACTORES INT,
CodigoSuperior_PERSONAJES INT,
CONSTRAINT PK_PERSONAJES PRIMARY KEY(Codigo),
CONSTRAINT PK_ACTORES_PERSONAJES FOREIGN KEY(Codigo_ACTORES) REFERENCES ACTORES(Codigo),
CONSTRAINT PK_PERSONAJES_PERSONAJES FOREIGN KEY(CodigoSuperior_PERSONAJES) REFERENCES PERSONAJES(Codigo)
);


