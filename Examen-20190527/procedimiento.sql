1 -> mostrar el numero total de peliculas agrupadas por la clasificacion por edades y su media de duracion de cada categoria

select film.rating, count(*), avg(length) from film group by rating;

2 -> mostrar el listado de todos los paises con su nombre, con su numero de ciudades y el numero de clientes que contengan 'FR' en el nombre del pais

select country.country, count(city.city), count(customer.customer_id) from country join city on city.country_id=country.country_id join address on city.city_id=address.city_id join customer on address.address_id=customer.address_id where country.country LIKE 'FR%' group by country.country;

3 -> mostrar por cada cliente activo de la tienda '2' con sus nombres y apellidos la cantidad gastada y el numero total de peliculas alquiladas

select customer.first_name, customer.last_name, SUM(amount), count(payment.customer_id) from customer join payment on customer.customer_id=payment.customer_id where active="1" && store_id="2" group by first_name,last_name;

4 -> realiza un procedimiento que muestre el tirulo, año de lanzamiento y el idioma de las peliculas que duren por encima de la media de peliculas

clausulas de comparacion
