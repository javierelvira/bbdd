DROP PROCEDURE IF EXISTS TablasFacturacion;
USE JARDINERIA;

DELIMITER |

CREATE PROCEDURE TablasFacturacion()

	BEGIN

	/*VARIABLES TABLA Facturas*/
	DECLARE CodigoCliente,CodigoFactura INT;
	DECLARE BaseImponible,IVA,Total NUMERIC (15.2);

	/*VARIABLES TABLA Comisiones*/
	DECLARE CodigoEmpleado INT;
	DECLARE Comision NUMERIC (15.2);

	/*VARIABLES TABLA AsientoContable*/
	DECLARE DEBE,HABER NUMERIC(15.2);

		/*TABLA AsientoContable*/
		DROP TABLE IF EXISTS AsientoContable;
        	CREATE TABLE AsientoContable (CodigoCliente INT, DEBE NUMERIC(15.2), HABER NUMERIC(15.2), FOREIGN KEY(CodigoCliente) REFERENCES Clientes(CodigoCliente));

/*                INSERT INTO AsientoContable VALUES(1,,);
                INSERT INTO AsientoContable VALUES(2,,);
                INSERT INTO AsientoContable VALUES(3,,);
                INSERT INTO AsientoContable VALUES(4,,);
                INSERT INTO AsientoContable VALUES(5,,);

		/*TABLA Facturas*/
		DROP TABLE IF EXISTS Facturas;
		CREATE TABLE Facturas (CodigoCliente INT, BaseImponible NUMERIC(15.2), IVA NUMERIC(15.2), Total NUMERIC(15.2), CodigoFactura INT, PRIMARY KEY(CodigoFactura), FOREIGN KEY(CodigoCliente) REFERENCES Clientes(CodigoCliente));

		/*TABLA Comisiones*/
		DROP TABLE IF EXISTS Comisiones;
		CREATE TABLE Comisiones (ID INT AUTO_INCREMENT, CodigoEmpleado INT, Comision NUMERIC(15.2), CodigoFactura INT, PRIMARY KEY(ID), FOREIGN KEY(CodigoFactura) REFERENCES Facturas(CodigoFactura));


	END;


    |

DELIMITER ;
