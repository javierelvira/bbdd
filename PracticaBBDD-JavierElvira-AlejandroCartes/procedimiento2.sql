delimiter |

DROP PROCEDURE IF EXISTS GenerarContabilidad;

CREATE PROCEDURE GenerarContabilidad()
  BEGIN

  DECLARE cnt, totalClientes, Cliente INT;
  DECLARE GastoCliente NUMERIC(15,2);

  SET cnt = 0;
  SET totalClientes = (SELECT COUNT(CodigoCliente) FROM Clientes);

  WHILE totalClientes >= cnt DO
	SET Cliente = (SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = cnt);

	IF Cliente IS NOT NULL THEN
		SET GastoCliente = (SELECT SUM(Total) AS 'Precio total' FROM Facturas WHERE CodigoCliente = cnt GROUP BY CodigoCliente);

	IF GastoCliente IS NOT NULL THEN
		INSERT INTO AsientoContable (CodigoCliente, DEBE, HABER) VALUES (Cliente, GastoCliente, NULL);

	END IF;
	END IF;

	SET cnt = cnt +1;

  END WHILE;

  END
  |
delimiter ;

