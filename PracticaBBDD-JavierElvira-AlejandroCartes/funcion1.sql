USE JARDINERIA;

DROP FUNCTION IF EXISTS ultimaFactura;

delimiter |

CREATE FUNCTION ultimaFactura() RETURNS VARCHAR(20)
BEGIN
    DECLARE mostrar VARCHAR(30);

    IF ( SELECT COUNT(Facturas.CodigoFactura) FROM Facturas) = 0 THEN SET mostrar = '1';
    ELSE SET mostrar := (SELECT Facturas.CodigoFactura FROM Facturas, Pedidos ORDER BY Pedidos.FechaPedido LIMIT 1);
    END IF;


    RETURN mostrar;
END
|

delimiter ;

