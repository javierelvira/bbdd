DROP PROCEDURE IF EXISTS LimiteCredito;
USE JARDINERIA;

delimiter |
/*Creamos el Procedimiento*/
CREATE PROCEDURE LimiteCredito(N_Cliente INT) /*Declaramos Cliente para que a la hora de llamar al procedimiento, indiquemos al cliente al cual se le va a incrementar en 15%*/
BEGIN
    /*Declaracion de variables*/
    DECLARE Fecha DATE;
    DECLARE CodigoClientes INT;
    DECLARE Credito FLOAT;
    DECLARE Porcentaje FLOAT;
    DECLARE Incremento DECIMAL (15,2);

    /*Creamos la tabla*/
    DROP TABLE IF EXISTS Limite_Credito;

    CREATE TABLE Limite_Credito (Fecha DATE, CodigoCliente INT, Incremento DECIMAL(15,2));

    SET Fecha = (SELECT CURDATE());
    SET CodigoClientes = (SELECT Clientes.CodigoCliente FROM Clientes WHERE Clientes.CodigoCliente = N_Cliente);
    SET Credito = (SELECT Clientes.LimiteCredito FROM Clientes WHERE Clientes.CodigoCliente = N_Cliente);
    SET Porcentaje = (Credito * 0.15);
    SET Incremento = Credito + Porcentaje;

    /*Indicamos que se actualize en el campo LimiteCredito, la suma del Incremento en el que se aplica el 15%*/
    UPDATE Clientes SET LimiteCredito = Incremento WHERE CodigoCliente = N_Cliente;

    /*Insertamos los datos en la tabla*/
    INSERT INTO Limite_Credito VALUES (Fecha, CodigoClientes, Porcentaje);
END;

|

delimiter ;

