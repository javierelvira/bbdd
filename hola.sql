DROP PROCEDURE IF EXISTS holaproc;
DROP FUNCTION IF EXISTS holafunc;

delimiter //

CREATE PROCEDURE holaProc(IN nombre VARCHAR(30))
BEGIN
SELECT (CONCAT('Hola ',nombre)) AS 'Salida Terminal';
END//

delimiter ;

CALL holaProc('Javier');

/* Funcion sql */
CREATE FUNCTION holafunc()
RETURNS VARCHAR(30)
RETURN 'Hola';  /* return devuelve un dato y returns devuelve el tipo de dato*/

SELECT holafunc();

