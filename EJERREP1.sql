-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: EJERREP1
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CONTIENE`
--

DROP TABLE IF EXISTS `CONTIENE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTIENE` (
  `contenedor` varchar(20) DEFAULT NULL,
  `contenido` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CONTIENE`
--

LOCK TABLES `CONTIENE` WRITE;
/*!40000 ALTER TABLE `CONTIENE` DISABLE KEYS */;
/*!40000 ALTER TABLE `CONTIENE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OBJETO`
--

DROP TABLE IF EXISTS `OBJETO`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OBJETO` (
  `nombre` varchar(20) NOT NULL,
  `tamaño` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OBJETO`
--

LOCK TABLES `OBJETO` WRITE;
/*!40000 ALTER TABLE `OBJETO` DISABLE KEYS */;
/*!40000 ALTER TABLE `OBJETO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PERSONA`
--

DROP TABLE IF EXISTS `PERSONA`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PERSONA` (
  `nombre` varchar(20) NOT NULL,
  `apellidos` varchar(20) NOT NULL,
  `trabajo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`nombre`,`apellidos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PERSONA`
--

LOCK TABLES `PERSONA` WRITE;
/*!40000 ALTER TABLE `PERSONA` DISABLE KEYS */;
/*!40000 ALTER TABLE `PERSONA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SITUACION`
--

DROP TABLE IF EXISTS `SITUACION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SITUACION` (
  `hora` time NOT NULL,
  `lugar` varchar(20) NOT NULL,
  `nombrePERSONA` varchar(20) DEFAULT NULL,
  `apellidosPERSONA` varchar(20) DEFAULT NULL,
  `vestuario` varchar(20) DEFAULT NULL,
  `mercancia` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`hora`,`lugar`),
  KEY `nombrePERSONA` (`nombrePERSONA`,`apellidosPERSONA`),
  CONSTRAINT `SITUACION_ibfk_1` FOREIGN KEY (`nombrePERSONA`, `apellidosPERSONA`) REFERENCES `PERSONA` (`nombre`, `apellidos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SITUACION`
--

LOCK TABLES `SITUACION` WRITE;
/*!40000 ALTER TABLE `SITUACION` DISABLE KEYS */;
/*!40000 ALTER TABLE `SITUACION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lleva`
--

DROP TABLE IF EXISTS `lleva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lleva` (
  `nombreOBJETO` varchar(20) DEFAULT NULL,
  `horaSITUACION` time DEFAULT NULL,
  `lugarSITUACION` varchar(20) DEFAULT NULL,
  KEY `nombreOBJETO` (`nombreOBJETO`),
  KEY `horaSITUACION` (`horaSITUACION`,`lugarSITUACION`),
  CONSTRAINT `lleva_ibfk_1` FOREIGN KEY (`nombreOBJETO`) REFERENCES `OBJETO` (`nombre`),
  CONSTRAINT `lleva_ibfk_2` FOREIGN KEY (`horaSITUACION`, `lugarSITUACION`) REFERENCES `SITUACION` (`hora`, `lugar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lleva`
--

LOCK TABLES `lleva` WRITE;
/*!40000 ALTER TABLE `lleva` DISABLE KEYS */;
/*!40000 ALTER TABLE `lleva` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-10 10:06:31
